package com.gd.models;

import com.gd.models.animals.Animal;
import com.gd.models.interfaces.SkyExplorer;
import com.gd.models.interfaces.LandExplorer;
import com.gd.models.interfaces.WaterExplorer;

import java.util.HashSet;

public class Farm {
    private HashSet<Animal> animalList = new HashSet<>();

    public Farm() {
    }

    public int getFarmAnimalCount() {
        return animalList.size();
    }

    public void addAnimal(Animal animal) {
        boolean animalIdExist = animalList.stream().anyMatch(currentAnimal -> animal.getId() == currentAnimal.getId());
        if (animalIdExist) {
            System.out.println("The animal id is already present");
        } else {
            animalList.add(animal);
        }
    }

    public long getFlyingAnimalsCount() {
        return animalList.stream().filter(vehicule -> vehicule instanceof SkyExplorer).count();
    }

    public long getSwimmingAnimalsCount() {
        return animalList.stream().filter(vehicule -> vehicule instanceof WaterExplorer).count();
    }

    public long getWalkingAnimalsCount() {
        return animalList.stream().filter(vehicule -> vehicule instanceof LandExplorer).count();
    }
    public void printAnimalCountInConsole() {
        System.out.println("---------------------------------------");
        System.out.println("Nombre de Vehicule: "  + animalList.size());
        System.out.println("---------------------------------------");
    }

    public void printFlyingAnimals() {
        System.out.println("-----------------Flying Animals----------------------");
        animalList.stream().filter(animal -> animal instanceof SkyExplorer).forEach(this::printAnimalInConsole);
        System.out.println("---------------------------------------");
    }

    public void printSwimmingAnimals() {
        System.out.println("-------------------Swmming Animals--------------------");
        animalList.stream().filter(animal -> animal instanceof WaterExplorer).forEach(this::printAnimalInConsole);
        System.out.println("---------------------------------------");
    }

    public void printWalkingAnimals() {
        System.out.println("------------------Walking Animals---------------------");
        animalList.stream().filter(animal -> animal instanceof LandExplorer).forEach(this::printAnimalInConsole);
        System.out.println("---------------------------------------");
    }

    public void printAnimalListInConsole() {
        System.out.println("---------------------------------------");
        animalList.forEach(this::printAnimalInConsole);
        System.out.println("---------------------------------------");
    }

    private void printAnimalInConsole(Animal animal) {
        System.out.println("Animal de type " + animal.getClass().getSimpleName());
        System.out.println(animal);
    }
}
