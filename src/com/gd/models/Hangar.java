package com.gd.models;

import com.gd.models.interfaces.SkyExplorer;
import com.gd.models.interfaces.LandExplorer;
import com.gd.models.interfaces.WaterExplorer;
import com.gd.models.vehicules.Vehicule;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;

public class Hangar {
    private HashSet<Vehicule> vehiculeList = new HashSet<>();

    public Hangar() {}

    public void addVehicule(Vehicule vehicule) {
        boolean vehiculeIdExist = vehiculeList.stream().anyMatch(currentVehicule -> vehicule.getId() == currentVehicule.getId());
        if(vehiculeIdExist) {
            System.out.println("The vehicule id is already present");
        } else {
            vehiculeList.add(vehicule);
        }
    }

    public long getHangarVehiculeCount() {
        return vehiculeList.size();
    }

    public long getAirportSectionVehiculesCount() {
        return vehiculeList.stream().filter(vehicule -> vehicule instanceof SkyExplorer).count();
    }

    public long getPortSectionVehiculesCount() {
        return vehiculeList.stream().filter(vehicule -> vehicule instanceof WaterExplorer).count();
    }

    public long getGarageSectionVehiculesCount() {
        return vehiculeList.stream().filter(vehicule -> vehicule instanceof LandExplorer).count();
    }

    public void printAirportSectionVehicules() {
        System.out.println("-----------------Airport Section----------------------");
        vehiculeList.stream().filter(vehicule -> vehicule instanceof SkyExplorer).forEach(this::printVehiculeInConsole);
        System.out.println("---------------------------------------");
    }

    public void printPortSectionVehicules() {
        System.out.println("------------------Port Section---------------------");
        vehiculeList.stream().filter(vehicule -> vehicule instanceof WaterExplorer).forEach(this::printVehiculeInConsole);
        System.out.println("---------------------------------------");
    }

    public void printGarageSectionVehicules() {
        System.out.println("-------------------Garage Section--------------------");
        vehiculeList.stream().filter(vehicule -> vehicule instanceof LandExplorer).forEach(this::printVehiculeInConsole);
        System.out.println("---------------------------------------");
    }

    public void printVehiculeListInConsole() {
        System.out.println("---------------------------------------");
        vehiculeList.forEach(this::printVehiculeInConsole);
        System.out.println("---------------------------------------");
    }

    public void printVehiculeListInFile(File file) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new FileOutputStream(file));
        vehiculeList.forEach(vehicule -> {
            printStream.println("Vehicule de type "  + vehicule.getClass().getSimpleName());
            printStream.println(vehicule);
        });
    }

    private void printVehiculeInFile(Vehicule vehicule, File file) throws FileNotFoundException {
        PrintStream printStream = new PrintStream(new FileOutputStream(file));
        printStream.println("Vehicule de type "  + vehicule.getClass().getSimpleName());
        printStream.println(vehicule);
    }

    public void printVehiculeCountInConsole() {
        System.out.println("---------------------------------------");
        System.out.println("Nombre de Vehicule: "  + vehiculeList.size());
        System.out.println("---------------------------------------");
    }

    private void printVehiculeInConsole(Vehicule vehicule) {
        System.out.println("Vehicule de type "  + vehicule.getClass().getSimpleName());
        System.out.println(vehicule);
    }
}
