package com.gd.models.vehicules;

import com.gd.models.interfaces.WaterExplorer;

public class JetSki extends Vehicule implements WaterExplorer {
    public JetSki(int id) {
        super(id);
    }

    @Override
    public String move() {
        return sail();
    }
}
