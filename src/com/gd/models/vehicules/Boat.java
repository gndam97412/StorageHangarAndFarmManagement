package com.gd.models.vehicules;

import com.gd.models.interfaces.WaterExplorer;

public class Boat extends Vehicule implements WaterExplorer {
    public Boat(int id) {
        super(id);
    }

    @Override
    public String move() {
        return sail();
    }
}
