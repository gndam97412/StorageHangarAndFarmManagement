package com.gd.models.vehicules;

import com.gd.models.interfaces.SkyExplorer;

public class Helicopter extends Vehicule implements SkyExplorer {

    public Helicopter(int id) {
        super(id);
    }

    @Override
    public String move() {
        return fly();
    }
}
