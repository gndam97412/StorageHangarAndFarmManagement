package com.gd.models.vehicules;

import com.gd.models.interfaces.SkyExplorer;

public class Plane extends Vehicule implements SkyExplorer {

    public Plane(int id) {
        super(id);
    }

    @Override
    public String move() {
        return fly();
    }
}
