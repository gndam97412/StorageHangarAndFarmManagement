package com.gd.models.vehicules;

import com.gd.models.interfaces.SkyExplorer;
import com.gd.models.interfaces.WaterExplorer;

public class SeaPlane extends Vehicule implements SkyExplorer, WaterExplorer {
    public SeaPlane(int id) {
        super(id);
    }

    @Override
    public String move() {
        return sail() + " and " + fly();
    }
}
