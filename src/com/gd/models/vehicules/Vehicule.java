package com.gd.models.vehicules;

public abstract class Vehicule {
    private int id;

    public Vehicule(int id) {
        this.id = id;
    }

    public abstract String move();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
