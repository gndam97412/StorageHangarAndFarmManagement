package com.gd.models.animals;

import com.gd.models.interfaces.WaterExplorer;

public class Salmon extends Animal implements WaterExplorer {
    public Salmon(int id) {
        super(id);
    }

    @Override
    public String move() {
        return sail();
    }
}
