package com.gd.models.animals;

public abstract class Animal {
    private int id;
    public abstract String move();
    public Animal(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
