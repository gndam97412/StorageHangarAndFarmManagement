package com.gd.models.animals;

import com.gd.models.interfaces.SkyExplorer;

public class Dove extends Animal implements SkyExplorer {
    public Dove(int id) {
        super(id);
    }

    @Override
    public String move() {
        return fly();
    }
}
