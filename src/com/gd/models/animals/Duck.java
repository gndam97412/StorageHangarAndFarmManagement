package com.gd.models.animals;

import com.gd.models.interfaces.SkyExplorer;
import com.gd.models.interfaces.LandExplorer;
import com.gd.models.interfaces.WaterExplorer;

public class Duck extends Animal implements SkyExplorer, WaterExplorer, LandExplorer {
    public Duck(int id) {
        super(id);
    }

    @Override
    public String move() {
        return fly() + ", " + sail() + " and " + walk();
    }
}
