package com.gd.models.animals;

import com.gd.models.interfaces.WaterExplorer;

public class Dolphin extends Animal implements WaterExplorer {
    public Dolphin(int id) {
        super(id);
    }

    @Override
    public String move() {
        return sail();
    }
}
