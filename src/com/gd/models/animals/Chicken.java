package com.gd.models.animals;

import com.gd.models.interfaces.LandExplorer;

public class Chicken extends Animal implements LandExplorer {
    public Chicken(int id) {
        super(id);
    }

    @Override
    public String move() {
        return walk();
    }
}
