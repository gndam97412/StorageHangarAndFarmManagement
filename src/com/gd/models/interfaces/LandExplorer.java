package com.gd.models.interfaces;

public interface LandExplorer {
    default String walk() {
        return "I can move on land";
    };
}
