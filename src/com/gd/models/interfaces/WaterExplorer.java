package com.gd.models.interfaces;

public interface WaterExplorer {
    default String sail(){
        return "I can move on the water";
    };
}
