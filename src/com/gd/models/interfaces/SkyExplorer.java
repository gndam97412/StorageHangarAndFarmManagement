package com.gd.models.interfaces;

public interface SkyExplorer {
    default String fly() {
        return "I can fly";
    }
}
