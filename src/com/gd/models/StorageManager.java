package com.gd.models;

public class StorageManager {
    private Hangar hangar;
    private Farm farm;

    public StorageManager() {}

    public Hangar getHangar() {
        return hangar;
    }

    public void setHangar(Hangar hangar) {
        this.hangar = hangar;
    }

    public Farm getFarm() {
        return farm;
    }

    public void setFarm(Farm farm) {
        this.farm = farm;
    }

    public void printAirExplorer() {
        System.out.println("---------------------------------------");
        hangar.printAirportSectionVehicules();
        farm.printFlyingAnimals();
        System.out.println("---------------------------------------");
    }

    public void printWaterExplorer() {
        System.out.println("---------------------------------------");
        hangar.printPortSectionVehicules();
        farm.printSwimmingAnimals();
        System.out.println("---------------------------------------");
    }

    public void printLandExplorer() {
        System.out.println("---------------------------------------");
        hangar.printGarageSectionVehicules();
        farm.printWalkingAnimals();
        System.out.println("---------------------------------------");
    }

    public void printAllStorage() {
        System.out.println("----------------Vehicules-----------------------");
        this.hangar.printVehiculeListInConsole();
        System.out.println("----------------Animals-----------------------");
        this.farm.printAnimalListInConsole();
    }

    public void printStorageCount() {
        System.out.println("---------------------------------------");
        System.out.println("Stored item count: " + (this.hangar.getHangarVehiculeCount() + this.farm.getFarmAnimalCount()));
        System.out.println("---------------------------------------");
    }

}
