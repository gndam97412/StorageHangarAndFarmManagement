package com.gd;

import com.gd.models.Hangar;
import com.gd.models.vehicules.*;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Implements the part 1 of the exercice
 */
public class Part1 {

    public static void main(String[] args) {
        Hangar hangar = createHangar();

        File newFile = new File("hangarContent.txt");

        hangar.printVehiculeCountInConsole();
        hangar.printVehiculeListInConsole();

        hangar.printGarageSectionVehicules();
        hangar.printAirportSectionVehicules();
        hangar.printPortSectionVehicules();
        try {
            hangar.printVehiculeListInFile(newFile);
        } catch (FileNotFoundException e) {
            System.out.println("An error occured while reading the file " + newFile.getName());
        }
    }

    /**
     * Create a {@link Hangar}
     *
     * @return the created {@link Hangar}
     */
    private static Hangar createHangar() {
        Hangar hangar = new Hangar();
        Bike bike = new Bike(1);
        Boat boat = new Boat(2);
        Car car = new Car(3);
        Helicopter helicopter = new Helicopter(4);
        JetSki jetSki = new JetSki(5);
        Plane plane = new Plane(6);
        SeaPlane seaPlane = new SeaPlane(7);
        hangar.addVehicule(bike);
        //Should produce a message in the console as a vehicule with this id already exists
        hangar.addVehicule(new Plane(1));
        hangar.addVehicule(boat);
        hangar.addVehicule(car);
        hangar.addVehicule(helicopter);
        hangar.addVehicule(jetSki);
        hangar.addVehicule(plane);
        hangar.addVehicule(seaPlane);
        return hangar;
    }
}
