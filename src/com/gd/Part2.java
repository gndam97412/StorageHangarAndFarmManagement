package com.gd;

import com.gd.models.Farm;
import com.gd.models.Hangar;
import com.gd.models.StorageManager;
import com.gd.models.animals.*;
import com.gd.models.vehicules.*;

/**
 * Implement the part 2 of the exercice
 */
public class Part2 {
    public static void main(String[] args) {
        Hangar hangar = createHangar();

        Farm farm = createFarm();

        /*farm.printAnimalListInConsole();
        farm.printAnimalCountInConsole();
        farm.printWalkingAnimals();
        farm.printFlyingAnimals();
        farm.printSwimmingAnimals();*/

        StorageManager storageManager = new StorageManager();
        storageManager.setFarm(farm);
        storageManager.setHangar(hangar);

        storageManager.printStorageCount();
        storageManager.printAirExplorer();
        storageManager.printLandExplorer();
        storageManager.printWaterExplorer();
    }

    /**
     * Method to create a {@link Farm}
     * @return the created {@link Farm}
     */
    private static Farm createFarm() {
        Farm farm = new Farm();
        Chicken chicken = new Chicken(1);
        Dolphin dolphin = new Dolphin(2);
        Dove dove = new Dove(3);
        Duck duck = new Duck(4);
        Salmon salmon = new Salmon(5);
        Salmon salmonTwo = new Salmon(1);
        farm.addAnimal(chicken);
        farm.addAnimal(dolphin);
        farm.addAnimal(dove);
        farm.addAnimal(duck);
        farm.addAnimal(salmon);
        //Should produce a message in the console as an animal with this id already exist
        farm.addAnimal(salmonTwo);
        return farm;
    }

    /**
     * Create a {@link Hangar}
     * @return the created {@link Hangar}
     */
    private static Hangar createHangar() {
        Hangar hangar = new Hangar();
        Bike bike = new Bike(1);
        Boat boat = new Boat(2);
        Car car = new Car(3);
        Helicopter helicopter = new Helicopter(4);
        JetSki jetSki = new JetSki(5);
        Plane plane = new Plane(6);
        SeaPlane seaPlane = new SeaPlane(7);
        hangar.addVehicule(bike);
        hangar.addVehicule(boat);
        hangar.addVehicule(car);
        hangar.addVehicule(helicopter);
        hangar.addVehicule(jetSki);
        hangar.addVehicule(plane);
        hangar.addVehicule(seaPlane);
        return hangar;
    }
}
